<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host=localhost;dbname=assignment1;charset=utf8mb4', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
	{
		$stmt = $this->db->query('SELECT * FROM book');
		
		$booklist = array();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
			array_push($booklist, new Book($row['title'], $row['author'], $row['description'], $row['id']));
		}
		
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
	 * @throws InvalidArgumentException
     */
    public function getBookById($id)
    {
		if(!is_numeric($id))
			throw new InvalidArgumentException('Invalid book id; expected an integer');
		
		$stmt = $this->db->prepare('SELECT * FROM book WHERE id=:id');
		$stmt->bindValue(':id', $id);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		if($row)
		{
			return new Book($row['title'], $row['author'], $row['description'], $row['id']);
		}
		return null;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
	 * @throws InvalidArgumentException
     */
    public function addBook($book)
    {
		if($book->title == '' || $book->author == '')
			throw new InvalidArgumentException('Invalid input; title and author is required');
		
		$stmt = $this->db->prepare('INSERT INTO book(title,author,description) VALUES(:title,:author,:description)');
		$stmt->bindValue(':title', $book->title);
		$stmt->bindValue(':author', $book->author);
		$stmt->bindValue(':description', $book->description);
		$stmt->execute();
		$book->id = $this->db->lastInsertId();
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
	 * @throws InvalidArgumentException
     */
    public function modifyBook($book)
    {
		if(!is_numeric($book->id))
			throw new InvalidArgumentException('Invalid book id; expected an integer');
		
		if($book->title == '' || $book->author == '')
			throw new InvalidArgumentException('Invalid input; title and author is required');
		
		$stmt = $this->db->prepare('UPDATE book SET title=:title, author=:author, description=:description  WHERE id=:id');
		$stmt->bindValue(':id', $book->id);
		$stmt->bindValue(':title', $book->title);
		$stmt->bindValue(':author', $book->author);
		$stmt->bindValue(':description', $book->description);
		$stmt->execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
	 * @throws InvalidArgumentException
     */
    public function deleteBook($id)
    {
		if(!is_numeric($id))
			throw new InvalidArgumentException('Invalid book id; expected an integer');

		$stmt = $this->db->prepare('DELETE FROM book WHERE id=:id');
		$stmt->bindValue(':id', $id);
		$stmt->execute();
    }
}
?>